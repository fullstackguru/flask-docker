FROM ubuntu:17.04

MAINTAINER Blue Discover

RUN groupadd -r webapps && useradd -r -g webapps test
RUN apt-get update && apt-get install -fy apache2\
    libapache2-mod-wsgi \
    apache2-dev \
    libjpeg8-dev \
    python2.7 \
    python-dev \
    python-pip \
    git \
    python-virtualenv \
    libmysqlclient-dev \
    libcurl4-openssl-dev \
    build-essential \
    libssl-dev \
    libffi-dev \
    ruby

RUN virtualenv --python=python2.7 /var/www/virtualenv

COPY ./requirements.txt /var/www/test/requirements.txt

RUN /var/www/virtualenv/bin/pip install -U setuptools pip
RUN /var/www/virtualenv/bin/pip install -r /var/www/test/requirements.txt

COPY deploy/apache.conf /etc/apache2/sites-available/apache.conf
RUN a2ensite apache
RUN a2enmod headers

# Copy wsgi file
COPY . /var/www/test
COPY deploy/config_prod.py /var/www/test/config.py

RUN chmod +x /var/www/test/site.wsgi
RUN chmod -R 777 /var/www/test/app/static
RUN chmod -R 777 /var/www/test/app/templates
RUN a2dissite 000-default.conf
RUN a2ensite apache.conf

EXPOSE 80

WORKDIR /var/www/test

CMD /usr/sbin/apache2ctl -D FOREGROUND