from datetime import date
from flask import Flask
# from flask.ext import breadcrumbs
import flask_breadcrumbs as breadcrumbs
from flask_debugtoolbar import DebugToolbarExtension
from flask.ext.compress import Compress
from flask_mail import Mail
from flask_security import current_user, Security, SQLAlchemyUserDatastore
from flask_sqlalchemy import SQLAlchemy
from jinja2.exceptions import UndefinedError
from slacker import Slacker

import pytz
import re
import stripe
import os

app = Flask(__name__)
app.config.from_object('config')
Compress(app)
db = SQLAlchemy(app)
mail = Mail(app)
breadcrumbs.Breadcrumbs(app=app)
toolbar = DebugToolbarExtension(app)
stripe.api_key = app.config['STRIPE_KEYS']['SK']

if 'SLACK_KEY' in app.config:
    slack = Slacker(app.config['SLACK_KEY'])

from app.models import User, Role
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

from . import views, models
from app.admin.views import mod_admin as admin_module
from app.user.views import mod_user as user_module
from app.util.assets import assets
app.register_blueprint(admin_module)
app.register_blueprint(user_module)


# Global Jinja2 filters

def get_year():
    return date.today().year


def format_currency(value, format='full'):
    if format == 'full':
        return "${:,.2f}".format(value)
    elif format == 'pretty':
        return "${:,.0f}".format(value)
    elif format == 'decimal':
        return "{:,.2f}".format(value)


def format_datetime(value, format='medium'):
    try:
        tz = pytz.timezone(current_user.time_zone)
        tzoffset = tz.utcoffset(value)
        tzvalue = value + tzoffset
        if format == 'full':
            return tzvalue.strftime('%c')
        elif format == 'pretty':
            return tzvalue.strftime('%m/%d/%Y at %I:%M %p')
        elif format == 'medium':
            return tzvalue.strftime('%m/%d/%Y')
        elif format == 'sort':
            return tzvalue.strftime('%s')
    except (AttributeError, TypeError):
        return ''


def format_phonenumber(value):
    try:
        clean_phone_number = re.sub('[^0-9]+', '', value)
        formatted_phone_number = re.sub("(\d)(?=(\d{3})+(?!\d))", r"\1-", "%d" % int(clean_phone_number[:-1])) + clean_phone_number[-1]
        return formatted_phone_number
    except (AttributeError, TypeError):
        return value

app.jinja_env.globals.update(get_year=get_year)
app.jinja_env.filters['datetime'] = format_datetime
app.jinja_env.filters['currency'] = format_currency
app.jinja_env.filters['phonenumber'] = format_phonenumber


# Initialize the app

if __name__ == '__main__':
    port = int(os.environ.get("PORT", 8888))
    app.run(host='0.0.0.0', port=port, debug=False)
