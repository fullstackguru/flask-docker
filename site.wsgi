#!/usr/bin/python
import sys
import logging

activate_this = '/var/www/virtualenv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, "/var/www/test")

from app import app
from werkzeug.debug import DebuggedApplication

application = DebuggedApplication(app, True)
application.secret_key = 'd6a2f8981c1248f257ebc0cb752322a98dd2150981'
