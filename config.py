# For dev/test only -- DO NOT DEPLOY!

DEBUG = True
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
BASE_URL = "https://test.com"

APP_NAME = "Test"
APP_STYLED_NAME = "Test"
COMPANY_NAME = "Test, LLC"
COMPANY_ADDRESS = "A, B, C"
COMPANY_PHONE = "(123) 456-890"
COMPANY_LINK = "http://www.test.com/"
CONTACT_LINK = "http://www.test.com/contact-us/"
OWNER_NAME = "Test, Inc"
OWNER_LINK = "http://test.com/"
# TODO change in production
CONTACT_EMAIL = "test@gmail.com"
ADMINS = ['test@gmail.com']

SQLALCHEMY_DATABASE_URI = "mysql://root@test.com/test"
SECURITY_PASSWORD_HASH = "sha512_crypt"
SECURITY_PASSWORD_SALT = "whirlpool"
SECURITY_POST_LOGIN_VIEW = "/redirect"
SECURITY_RECOVERABLE = True
SECURITY_CHANGEABLE = True
SECURITY_CONFIRMABLE = True
SECURITY_REGISTERABLE = True
SECURITY_TRACKABLE = True
SECURITY_EMAIL_SENDER = (APP_NAME, "no-reply@test.com")
THREADS_PER_PAGE = 2
CSRF_ENABLED = True
CSRF_SESSION_KEY = ""
SECRET_KEY = ""

# Change STRIPE_TEST to False to enable links to Live panel
STRIPE_TEST = True
STRIPE_KEYS = {
    'SK': "",
    'PK': ""
}

# Enable for S3 uploads
AMAZON_WEB_SERVICES_KEYS = {
    'ACCESS_KEY': "",
    'SECRET_ACCESS_KEY': "",
    'REGION_NAME': "us-west-2",
    'BUCKET': "dev"
}

ANALYTICS_TOKEN = "UA-57602751-1"

RECAPTCHA_PUBLIC_KEY = "6LfZbDUUAAAAAMyEYxSlp3mBj60AerO957XLMaTF"
RECAPTCHA_PRIVATE_KEY = "6LfZbDUUAAAAAE6T69moWhlB4xbuXiQT47kJgaHu"
RECAPTCHA_API_SERVER = "https://www.google.com/recaptcha/api.js"

# MAIL_SERVER = 'smtp.mailgun.org'
# MAIL_PORT = 587
# MAIL_USE_SSL = False
# MAIL_USERNAME = 'postmaster@sandbox6b854ff3e65843df954fcf8883ed283f.mailgun.org'
# MAIL_PASSWORD = '6aaed2b735651250db7253f253e9d23e'

MAIL_SERVER = 'mx.test.net'
MAIL_PORT = 587
MAIL_USE_SSL = False
MAIL_USERNAME = 'no-reply@test.net'
MAIL_PASSWORD = 'testpass'

SECURITY_EMAIL_SUBJECT_REGISTER = "Your new %s account" % APP_NAME
SECURITY_EMAIL_SUBJECT_PASSWORD_NOTICE = "%s password reset" % APP_NAME
SECURITY_EMAIL_SUBJECT_PASSWORD_RESET = "%s password reset confirmation" % APP_NAME
SECURITY_EMAIL_SUBJECT_PASSWORD_CHANGE_NOTICE = "%s password change confirmation" % APP_NAME
SECURITY_EMAIL_SUBJECT_CONFIRM = "Please confirm your %s account" % APP_NAME

SLACK_KEY = "123asd12eaqsdqwe12asd"

REPORT_IMAGE = 'https://test.com/images/test.jpg'

DEBUG_TB_INTERCEPT_REDIRECTS = False
